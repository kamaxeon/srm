# SRM

SRM (Storage rooms manager) is a simple tool to avoid my [Diogenes syndrome](https://en.wikipedia.org/wiki/Diogenes_syndrome).

## Why 

I have some storage rooms, and every storage room has shelves. Shelves storage boxes or items, so a box contains items.

I want an easy way to know where items are in the future. And I was looking for a killer feature I saw in a previous company many years ago. All boxes had an [EOL](https://en.wikipedia.org/wiki/End-of-life_product) sticker. Through those stickers, they were able to clean rooms.

Currently, I'm getting out of space, and usually, I cannot find items. So often, I buy things that I previously had storage 🤦.

## How

This project started during my Christmas holidays without pressure, trying to use all good practices. So maybe this project will be abandoned for a time or never end.

## Installation

TO BE DONE

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GPL 2](LICENSE)
